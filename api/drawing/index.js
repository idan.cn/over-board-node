const express = require('express');
const controller = require('./drawing.controller.js');
const router = express.Router();

router.get('/', controller.getAllDrawings);
router.get('/:id', controller.getDrawing);
router.post('/', controller.createDrawing);

module.exports = router;
