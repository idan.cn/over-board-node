const db = require('../../db');
const uuid = require('uuid/v4');

const getAllDrawings = () => {
	return db.query('select * from drawings where isPublic = 1');
}

const getDrawingById = (drawingId) => {
	return db.query('select * from drawings where drawingId = ?', drawingId);
}

const createDrawing = (drawing) => {
	const drawingId = uuid();
	drawing.drawingId = drawingId;
	drawing.drawingSegments = JSON.stringify(drawing.drawingSegments);

	return db.query('insert into drawings set ?', drawing);
}

module.exports = {
	getDrawingById,
	getAllDrawings,
	createDrawing
}