const _ = require('lodash');
const drawingService = require('./drawing.service');

const getAllDrawings = (req, res) => {
	drawingService.getAllDrawings()
		.then(drawings => {
			res.json(drawings);
		})
		.catch(err => {
			res.status(500).json(err);
		})
}

const getDrawing = (req, res) => {
	
	if (!req.params.id) {
		res.status(400).json({ error: 'drawing id param is missing' })
		return
	}
	
	drawingService.getDrawingById(req.params.id)
		.then(drawings => {
			if (!drawings[0]) {
				res.status(404).json({ error: 'drawing not found' });
			}
			else {
				drawings[0].drawingSegments = JSON.parse(drawings[0].drawingSegments);
				res.json(drawings[0]);
			}
		})
		.catch(err => {
			res.status(500).json(err);
		})
}

const createDrawing = (req, res) => {
	const drawing = req.body;

	if (!drawing.creationDate || !drawing.dataUrl || !drawing.drawDuration || !drawing.drawingSegments) {
		res.staus(400).json('error: some of the following params are missing: creationDate, dataUrl, drawDuration, drawingSegments')
		return
	}
	
	drawingService.createDrawing(drawing)
		.then(() => {
			res.json(drawing);
		})
		.catch(err => {
			res.status(500).json(err);
		})
}


module.exports = {
	getAllDrawings,
	createDrawing,
	getDrawing
};