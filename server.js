const express = require("express");
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({
	extended: true,
	limit: '30mb',
}));
app.use(bodyParser.json({limit: '30mb'}));

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});
  
app.use('/api/drawing', require('./api/drawing'));
app.use('/api/v1/get-feed-data', (req, res) => {
	res.json({reserveWithGoogleData: {
		mainReserveUrl: {url: 'https://audriusk.wixsite.com/bookings-demo/service-page/eye-tests'},
		services: [{
			id:"0a6c04ce-4aa9-4548-a70c-f1f03c16a35d",
			name:"my amazing yoga class",
			category:"yoga",
			description:"come and get your body refreshed",
			fixedPrice: {currency: "USD", value: "12.3"},
			fixedDuration: "100",
			serviceReserveUrl: {url:"https://audriusk.wixsite.com/bookings-demo/service-page/eye-tests"},
		}]
	}})
});

const port = process.env.PORT || 5000;
app.listen(port, function() {
  console.log("Listening on " + port);
});

process.on('uncaughtException', (err) => {
    console.log('uncaughtException:', err);
    process.exit(1);
});